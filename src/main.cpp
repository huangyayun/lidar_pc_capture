#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <dirent.h>
#include <fstream>
#include <boost/filesystem.hpp>

//set the recording path
std::string main_path = "/tmp/record";

//define the point cloud data structure
struct PointXYZIT {
PCL_ADD_POINT4D; // quad-word XYZ
double timestamp;
uint16_t intensity;
uint8_t flags;
uint8_t elongation;
uint16_t scan_id;
uint16_t scan_idx;
#if 0
float g_angle;
uint16_t cluster;
#endif
EIGEN_MAKE_ALIGNED_OPERATOR_NEW // ensure proper alignment
} EIGEN_ALIGN16;

POINT_CLOUD_REGISTER_POINT_STRUCT (PointXYZIT,  
                                   (float, x, x) (float, y, y)
                                   (float, z, z) (double, timestamp, timestamp )
                                   (uint16_t, intensity, intensity) (uint8_t, flags, flags)
                                   (uint8_t,elongation,elongation)
                                   (uint16_t, scan_id, scan_id) (uint16_t, scan_idx, scan_idx )
)

typedef PointXYZIT iVuPoint;
typedef pcl::PointCloud<iVuPoint> iVuPointCloud;

void lidar_callback(const sensor_msgs::PointCloud2::ConstPtr &msg)
{
    iVuPointCloud cloud;
    pcl::fromROSMsg(*msg, cloud);

    // timestamp as folder name
    long timestamp = msg->header.stamp.sec * 1e3 + msg->header.stamp.nsec / 1e6;
    time_t rawtime = msg->header.stamp.sec;
    struct tm *timeinfo = localtime(&rawtime);
    char str_time[100];
    sprintf(str_time, "%04d%02d%02d%02d%02d", timeinfo->tm_year+1900, timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min);
    std::string sub_folder(str_time);

    //create folder
    std::string path = main_path + "/" + sub_folder + "/lidar/";
    boost::filesystem::create_directories(path);

    //pcd file name
    std::string pcd_name = path + std::to_string(timestamp)+ ".pcd";

    pcl::io::savePCDFile(pcd_name, cloud, true);

}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "lidar_pc_capture");
    ros::NodeHandle nh;
    ros::NodeHandle nh_private("~");
    
    //ros parameter handle
    nh_private.param("record_path",main_path, std::string("/tmp/record"));

    std::string lidar_topic = "iv_points";
    
    ros::Subscriber lidar_sub = nh.subscribe(lidar_topic, 15, lidar_callback);

    ros::spin();
    return 0;
}